<?php

namespace End01here\EasySms;


/**
 * 获取短信发送服务对象
 * Class EasySmsService
 * @package End01here\EasySms
 */
class EasySmsService
{
    static private $sms_server = '';
    static private $code_server = '';

    /**
     * 获取短信发送服务
     * @return SmsServer|string
     */
    static function getSmsServer()
    {
        if (!self::$sms_server) {
            self::$sms_server = new SmsServer();
        }
        return self::$sms_server;
    }

    /**
     * 获取验证码服务
     * @param PhoneNumberServer $phone
     * @param string $code_type
     * @param string $code_num
     * @return CodeSever
     */
    static function getCodeSever(PhoneNumberServer $phone, $code_type = 'send_code', $code_num = '4')
    {
        return new CodeSever($phone, $code_type, $code_num);
    }

    /**
     * 获取手机号码对象
     * @param $phone
     * @param string $IDDCode
     * @return PhoneNumberServer
     */
    static function getPhoneSever($phone, $IDDCode = '86')
    {
        return new PhoneNumberServer($phone, $IDDCode);
    }

    /**
     * 获取消息对象
     * @return MessageServer
     */
    static function getMessageServer()
    {
        return new MessageServer();
    }


}
