<?php


namespace End01here\EasySms\Console;


use End01here\EasySms\CodeSever;
use Illuminate\Console\Command;

class ClearFileCodeCommand extends Command
{
    /**
     * The name of command.
     *
     * @var string
     */
    protected $name = 'easy:clear_code';

    /**
     * 清除过期文件缓存
     *
     * @var string
     */
    protected $description = '清除过期文件缓存';



    /**
     * Execute the command.
     *
     * @see fire()
     * @return void
     */
    public function handle(){
        $this->info('start--------');
        CodeSever::clearCode();
        $this->info('end--------');
    }





}
