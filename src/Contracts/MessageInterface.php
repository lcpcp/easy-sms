<?php

/*
 * This file is part of the overtrue/easy-sms.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace End01here\EasySms\Contracts;

/**
 * Interface MessageInterface.
 */
interface MessageInterface
{
    const TEXT_MESSAGE = 'text';

    const VOICE_MESSAGE = 'voice';

    /**
     *获取消息类型
     * @return string
     */
    public function getMessageType();

    /**
     * 获取文本内容
     * @return string
     */
    public function getContent();

    /**
     * @param $content
     * @return mixed
     */
    public function setContent(string $content);

    /**
     * 获取消息类型
     * @return string
     */
    public function getTemplate();

    /**
     * 设置模板编号
     * @param $template
     * @return mixed
     */
    public function setTemplate($template);

    /**
     * 获取数据集
     * @return array
     */
    public function getData();

    /**
     * 设置数据集
     * @param array $data
     * @return mixed
     */
    public function setData(array $data);
}
