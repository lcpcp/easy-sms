<?php

/*
 * This file is part of the overtrue/easy-sms.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace End01here\EasySms\Gateways;


use End01here\EasySms\Contracts\MessageInterface;
use End01here\EasySms\Contracts\PhoneNumberInterface;
use End01here\EasySms\Exceptions\GatewayErrorException;
use End01here\EasySms\Traits\HasHttpRequest;

/**
 * Class AliyunGateway.
 *
 * @author carson <docxcn@gmail.com>
 *
 * @see https://help.aliyun.com/document_detail/55451.html
 */
class WddzGateway extends Gateway
{
    use HasHttpRequest;

    const ENDPOINT_URL = 'http://api.wddz.xyz:8001/sms/api/sendMessage';


    public function send(PhoneNumberInterface $to, MessageInterface $message )
    {
        //获取签名
        $signName = $this->config['sign_text'];

        //组装请求数据
        $msg=$message->getContent();
        $sendtime = time()*1000;
        $sign=$this->sign($msg,$sendtime);
        $post_data = [
            'userName'=>$this->config['wddz']['username'],
            'content'=>$msg,
            'phoneList'=>[$to->getNumber()],
            'timestamp'=>$sendtime,
            'sign'=>$sign,
        ];
        //发起请求
        $result = $this->postJson(self::ENDPOINT_URL,json_encode($post_data));
        //解析返回信息sadasdsfdsf
        if (isset($result['code']) && $result['code']!=0) {
            throw new GatewayErrorException($result['message'], $result['code'], $result);
        }

        return ['code'=>'1','msg'=>'短信发送成功'];
    }

    function sign($msg,$sendtime){
        $post_data = $this->config['wddz']['username'].$msg.$sendtime.md5($this->config['wddz']['password']);
        return md5($post_data);
    }



}
