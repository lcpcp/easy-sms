<?php

namespace End01here\EasySms;


use End01here\EasySms\Contracts\MessageInterface;
use End01here\EasySms\Exceptions\MessageException;

/**
 * 获取消息对象
 * Class EasySmsService
 * @package End01here\EasySms
 */
class MessageServer implements MessageInterface
{

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $content;

    /**
     * @var string
     */
    protected $template;

    /**
     * @var array
     */
    protected $data = [];

    /**
     * Message constructor.
     *
     * @param string $type
     */
    public function __construct($type = MessageInterface::TEXT_MESSAGE)
    {
        $this->type = $type;
    }

    /**
     * Return the message type.
     *
     * @return string
     */
    public function getMessageType()
    {
        return $this->type;
    }

    /**
     * 获取消息内容
     * @return string
     */
    function getContent()
    {
        if (empty($this->content)) {
            throw new MessageException('消息内容未设置', 0);
        }
        return $this->content;
    }


    /**
     * 设置消息内容
     * @param string $content
     * @return mixed|void
     * @throws MessageException
     */
    function setContent(string $content)
    {
        if (empty($content)) {
            throw new MessageException('短信消息不能为空', 0);
        }
        $this->content = $content;
    }


    function setTemplate($template)
    {
        // TODO: Implement setTemplate() method.
        if (empty($template)) {
            throw new MessageException('模板信息不能为空', 0);
        }
        $this->template = $template;
    }

    function getTemplate()
    {
        // TODO: Implement getTemplate() method.
        if (empty($this->template)) {
            throw new MessageException('模板信息未设置', 0);
        }
        return $this->template;
    }

    function getData()
    {
        // TODO: Implement getData() method.
        return $this->data;
    }

    function setData(array $data)
    {
        // TODO: Implement setData() method.
        $this->data = $data;
    }

}
