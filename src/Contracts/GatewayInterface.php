<?php

/*
 * This file is part of the overtrue/easy-sms.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace End01here\EasySms\Contracts;



/**
 * Class GatewayInterface.
 */
interface GatewayInterface
{

    /**
     * Send a short message.
     *
     * @param \End01here\EasySms\Contracts\PhoneNumberInterface $to
     * @param \End01here\EasySms\Contracts\MessageInterface     $message
     *
     * @return array
     */
    public function send(PhoneNumberInterface $to, MessageInterface $message);
}
