<?php


use End01here\EasySms\EasySmsService;

class AliyunGatewayTest
{
    /**
     * 短信发送
     * @throws \End01here\EasySms\Exceptions\CodeErrorException
     * @throws \End01here\EasySms\Exceptions\GatewayErrorException
     * @throws \End01here\EasySms\Exceptions\MessageException
     */
    public function testSend()
    {
        $phone = EasySmsService::getPhoneSever('13600136001');
        $code_server = EasySmsService::getCodeSever($phone)->setCode();
        $message_server = EasySmsService::getMessageServer();
        $message_server->setTemplate('SMS_12975489');
        $message_server->setData(['name' => '张三', 'num' => $code_server->getCode()]);
        $sms_server = EasySmsService::getSmsServer()->creatAliSms()->send($phone, $message_server);
        dd($sms_server);
    }

    /**
     * 验证码验证
     */
    public function testVerify()
    {
        $phone = EasySmsService::getPhoneSever('13600136001');
        $ret_code = EasySmsService::getCodeSever($phone)->verifyCode('123456');
        dd($ret_code);
    }


}
