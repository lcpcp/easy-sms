<?php

namespace End01here\EasySms;


use End01here\EasySms\Gateways\AliyunGateway;
use End01here\EasySms\Gateways\KewailGateway;
use End01here\EasySms\Gateways\PuXinGateway;
use End01here\EasySms\Gateways\QyborGateway;
use End01here\EasySms\Gateways\SmsBaoGateway;
use End01here\EasySms\Gateways\WddzGateway;

/**
 * 获取短信发送服务对象
 * Class EasySmsService
 * @package End01here\EasySms
 */
class SmsServer
{

    private $ali_sms;

    /**
     * 创建阿里云短信接口对象
     */
    function creatAliSms()
    {
        if (empty($this->ali_sms)) {
            $this->ali_sms = new AliyunGateway();
        }
        return $this->ali_sms;
    }

    /**
     * 莫杜云
     * @var
     */
    private $kewail_sms;

    function creatKewailSms(){
        if (empty($this->kewail_sms)) {
            $this->kewail_sms = new KewailGateway();
        }
        return $this->kewail_sms;
    }


    /**
     * 企业宝
     * @var
     */
    private $qyb_sms;

    function creatQybSms(){
        if (empty($this->qyb_sms)) {
            $this->qyb_sms = new QyborGateway();
        }
        return $this->qyb_sms;
    }



    private $px_sms;

    function creatPxSms(){
        if (empty($this->px_sms)) {
            $this->px_sms = new PuXinGateway();
        }
        return $this->px_sms;
    }


    /**
     * 蔚岛云短信
     * @var
     */
    private $wddz_sms;

    function creatWddzSms(){
        if (empty($this->px_sms)) {
            $this->px_sms = new WddzGateway();
        }
        return $this->px_sms;
    }


    /**
     * 获取短信宝短信发送
     * @var
     */
    private $dx_sms;

    function creatSmsBao(){
        if (empty($this->dx_sms)) {
            $this->dx_sms = new SmsBaoGateway();
        }
        return $this->dx_sms;
    }

}
